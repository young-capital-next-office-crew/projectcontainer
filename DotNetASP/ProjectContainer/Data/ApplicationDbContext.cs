﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectContainer.Data;

namespace ProjectContainer.Data
{
	public class ApplicationDbContext : DbContext
	{

		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{ }

		// Define DbSet (table) of Problems in the database
		public DbSet<Problem> Problems { get; set; }
	}
}
