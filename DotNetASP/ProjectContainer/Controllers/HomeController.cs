﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectContainer.Models;

namespace ProjectContainer.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			// No further processing required, return index view
			return View();
		}

		public IActionResult Static(string page)
		{
			// Send the path of the requested page as model to the static view
			return View(nameof(Static), page);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			// Build the error view model and sned to error view
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
