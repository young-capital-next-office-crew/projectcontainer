﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectContainer.Models
{
	public class ProblemModel
	{
		public class IndexItem
		{
			public int Id { get; set; }

			[Display(Name = "Problem")]
			public string Name { get; set; }
		}

		public class CreateView
		{
			[Display(Name = "Problem")]
			public string Name { get; set; }
		}

		public class CreateInput
		{
			[Required]
			[MaxLength(100)]
			public string Name { get; set; }
		}

		public class EditView
		{
			public int Id { get; set; }

			[Display(Name = "Problem")]
			public string Name { get; set; }
		}

		public class EditInput
		{
			public int Id { get; set; }

			[Required]
			[MaxLength(100)]
			public string Name { get; set; }
		}

		public class DeleteView
		{
			public int Id { get; set; }

			public string Name { get; set; }
		}
	}
}
