﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectContainer.Migrations
{
    public partial class removePickCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PickCount",
                table: "Problem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PickCount",
                table: "Problem",
                nullable: false,
                defaultValue: 0);
        }
    }
}
