﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectContainer.Data;
using ProjectContainer.Models;

// Define ProblemModel as Model in this file, just because it is shorter (and more readable?)
using Model = ProjectContainer.Models.ProblemModel;

namespace ProjectContainer.Controllers
{
    public class ProblemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProblemsController(ApplicationDbContext context)
        {
			// Set the context to be easily accessible within this controller
            _context = context;
        }

        // GET: Problems
        public async Task<IActionResult> Index()
        {
			// Get all entries from the database translated to the index model class
			var model = await _context.Problems
				.Select(i => new Model.IndexItem()
				{
					Id = i.Id,
					Name = i.Name
				}).ToListAsync();

			// Send the collection of index models to the Index view
            return View(model);
        }

        // GET: Problems/Details/5
        public /*async Task<*/IActionResult/*>*/ Details(int? id)
        {
			// Currently problem is not elaborate enough to gain functionality from details
			// Redirect to index
			return RedirectToAction(nameof(Index));

			/*
			// Check if a valid id is given as parameter
            if (id == null)
                return NotFound();

			// Search the database for an entry with the given id
            var problem = await _context.Problems
                .FirstOrDefaultAsync(m => m.Id == id);

			// Check if the search had any results
            if (problem == null)
                return NotFound();

			// TODO: Translate the result to a details model

			// Send the result to the Details view
            return View(problem);
			*/
        }

        // GET: Problems/Create
        public IActionResult Create()
        {
			// Create an empty create model
			// You could already fill some fields here, for instance a datetime already being set to today
			var model = new Model.CreateView();

			// Send the model to the create view
            return View(model);
        }

        // POST: Problems/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name")] Model.CreateInput inputModel) // Only bind the Name, you want the database to set the id
        {
			// Check if the model meets the requirements, these can be set in the inputmodel class with attributes like [Required] or [MaxLength(100)]
            if (ModelState.IsValid)
            {
				// Id is automatically set by database
				Problem problem = new Problem() { Name = inputModel.Name };

				// Instruct the context to add the new problem object
                _context.Add(problem);

				// Save the changes and go back the the Index overview
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

			// Create viewmodel with inputmodel values, this way you get the resulting errors but you keep the user input
			// Just redirecting to GET: Problems/Create will clear all fields and not show errors
			Model.CreateView viewModel = new Model.CreateView() { Name = inputModel.Name };
			return View(viewModel);
        }

        // GET: Problems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
			// Check if a valid id was given as parameter
            if (id == null)
                return NotFound();

			// Search the database for an entry with the given id
            var problem = await _context.Problems.FindAsync(id);

			// Check if the search had a result
            if (problem == null)
                return NotFound();

			// Translate the result to an edit view model
			Model.EditView model = new Model.EditView() { Id = problem.Id, Name = problem.Name };

			// Send the result to the edit view
            return View(model);
        }

        // POST: Problems/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Model.EditInput inputModel)
        {
			// Check if the id given as parameter is the same as the id of the edit input model
            if (id != inputModel.Id)
                return NotFound();

			// Check if the model is valid according to the requirements, you can set these with attributes in the edit input model
            if (ModelState.IsValid)
            {
				// With this simplicity you are able to create a new object from edit
				// More complex data might require fetching the entry from the database and appling the values from inputmodel to it
				Problem problem = new Problem() { Id = inputModel.Id, Name = inputModel.Name };

				// try to update the entry in the database and save changes
                try
                {
                    _context.Update(problem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
					// Check if the error is that the entry does not exist
                    if (!ProblemExists(problem.Id))
                        return NotFound();
					else // Otherwise throw the error + stack
						throw;
                }

				// If update was successful, return to index
                return RedirectToAction(nameof(Index));
            }

			// If modelstate was not valid, return to the GET: Problems/Edit
			// Translate inputmodel to view model
			Model.EditView viewModel = new Model.EditView() { Id = inputModel.Id, Name = inputModel.Name };

            return View(viewModel);
        }

        // GET: Problems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
			// Check if a valid id was given as parameter
            if (id == null)
                return NotFound();

			// Search the database for an entry with matching id
            var problem = await _context.Problems
                .FirstOrDefaultAsync(m => m.Id == id);

			// Check if the search had any result
            if (problem == null)
                return NotFound();

			// Translate the result to a delete view model
			Model.DeleteView model = new Model.DeleteView() { Id = problem.Id, Name = problem.Name };

			// Send the result to the Delete view
            return View(model);
        }

        // POST: Problems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
			// Find the entry in the database
            var problem = await _context.Problems.FindAsync(id);

			// Remove the found entry in the database
            _context.Problems.Remove(problem);

			// Save changes to the database and return to index
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProblemExists(int id)
        {
			// Check if problem with id exists in the database
            return _context.Problems.Any(e => e.Id == id);
        }
    }
}
